<?php

/**
 * Implements hook_drush_command().
 */
function drush_toolbox_drush_command() {
  $items[] = array();
  $items['implements'] = array(
    'description' => 'List all modules that implement given hook.',
    'callback' => 'drush_toolbox_drush_implements',
  );
  return $items;
}

/**
 * Implements hook_drush_help().
 */
function drush_toolbox_drush_help($section) {
  switch ($section) {
    case 'drush:implements':
      return dt('List all modules that implement given hook.');
  }
}

/**
 * Callback for implements drush command.
 */
function drush_toolbox_drush_implements($hook) {
  $implementations = module_implements($hook, TRUE);
  drush_log(dt("Modules that implement %hook", array('%hook' => $hook)));
  foreach ($implementations as $module) {
    drush_log(dt("%module", array('%module' => $module)));
  }
}
